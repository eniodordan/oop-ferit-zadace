#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <fstream>
#include "description.h"
#include "episode.h"

using namespace std;

Description::Description() : mNumber(0), mLenght(0), mName("")
{
}

Description::Description(int number, int lenght, string name) : mNumber(number), mLenght(lenght), mName(name)
{
}

std::ostream& operator<< (std::ostream& OutputStream, const Description& rhs) {
	OutputStream << rhs.mNumber << "," << rhs.mLenght << "," << rhs.mName;

	return OutputStream;
}

std::istream& operator>> (std::istream& InputStream, Description& rhs) {
	char notch;
	InputStream >> rhs.mNumber >> notch >> rhs.mLenght >> notch;
	getline(InputStream, rhs.mName);

	return InputStream;
}

bool operator> (const Description& lhs, const Description& rhs)
{
	return lhs.mName[0] > rhs.mName[0];
}

Episode::Episode() : mViewers(0), mSumScore(0), mMaxScore(0)
{
}

Episode::Episode(int viewers, double sumscore, double maxscore, Description description) : mViewers(viewers), mSumScore(sumscore), mMaxScore(maxscore), mDescription(description)
{
}

std::ostream& operator<< (std::ostream& OutputStream, const Episode& rhs) {
	OutputStream << rhs.mViewers << " ," << rhs.mSumScore << "," << rhs.mMaxScore << "," << rhs.mDescription;

	return OutputStream;
}

std::istream& operator>> (std::istream& InputStream, Episode& rhs) {
	char notch;
	InputStream >> rhs.mViewers >> notch >> rhs.mSumScore >> notch >> rhs.mMaxScore >> notch >> rhs.mDescription;

	return InputStream;
}

bool operator> (const Episode& lhs, const Episode& rhs)
{
	return lhs.mDescription > rhs.mDescription;
}

void Episode::addView(float score) {
	mViewers++;
	mSumScore += score;

	if (score > mMaxScore) {
		mMaxScore = score;
	}
}

double Episode::getMaxScore() const {
	return mMaxScore;
}

double Episode::getAverageScore() const {
	return (float)(mSumScore / mViewers);
}

int Episode::getViewerCount() const {
	return mViewers;
}

float generateRandomScore() {
	float random = (float)rand() * 10 / (float)RAND_MAX;
	return random;
}

void print(Episode *episode[], const int count) {
	for (int i = 0; i < count; i++) {
		cout << i << ". " << *episode[i] << endl;
	}
}

void swap(Episode *xp, Episode *yp)
{
	Episode temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void sort(Episode *episode[], const int count) {
	int i, j;
	for (i = 0; i < count - 1; i++) {
		for (j = 0; j < count - i - 1; j++) {
			if (*episode[j] > *episode[j + 1]) {
				swap(episode[j], episode[j + 1]);
			}
		}
	}
}

void persistToFile(string fileName, Episode *episode[], const int count) {
	ofstream output(fileName);
	if (output.is_open() == false) {
		cout << "Error!" << endl;
	}
	else {
		for (int i = 0; i < count; i++) {
			output << i << ". " << *episode[i] << "\n";
		}
	}
	output.close();
}