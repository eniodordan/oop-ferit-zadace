#include <iostream>
#include <fstream>
#include "description.h"
#include "episode.h"

int main() {
	Description description(1, 45, "Pilot");
	std::cout << description << std::endl;
	Episode episode(10, 88.64, 9.78, description);
	std::cout << episode << std::endl;

	std::string fileName("shows.txt");
	const int COUNT = 10;

	std::ifstream input(fileName);
	Episode* episodes[COUNT];

	if (input.is_open() == false)
		return 1;

	for (int i = 0; i < COUNT; i++) {
		episodes[i] = new Episode();
		input >> *episodes[i];
	}
	input.close();

	std::cout << "Episodes:" << std::endl;
	print(episodes, COUNT);
	sort(episodes, COUNT);
	std::cout << "Sorted episodes:" << std::endl;
	print(episodes, COUNT);

	persistToFile("sorted.txt", episodes, COUNT);

	for (int i = 0; i < COUNT; i++) {
		delete episodes[i];
	}

	return 0;
}