#ifndef EPISODE_H
#define EPISODE_H
#define _CRT_SECURE_NO_WARNINGS

class Episode {
	friend std::ostream& operator<< (std::ostream&, const Episode&);
	friend std::istream& operator>> (std::istream&, Episode&);
	friend bool operator> (const Episode&, const Episode&);
private:
	int mViewers;
	double mSumScore;
	double mMaxScore;
	Description mDescription;
public:
	void addView(float);
	double getMaxScore() const;
	double getAverageScore() const;
	int getViewerCount() const;
	Episode();
	Episode(int, double, double, Description);
};

float generateRandomScore();
void print(Episode**, const int);
void swap(Episode*, Episode*);
void sort(Episode**, const int);
void persistToFile(std::string, Episode**, const int);

#endif