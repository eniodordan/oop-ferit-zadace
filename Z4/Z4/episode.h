#ifndef EPISODE_H
#define EPISODE_H
#define _CRT_SECURE_NO_WARNINGS

class Episode {
	friend std::ostream& operator<< (std::ostream&, const Episode&);
	friend std::istream& operator>> (std::istream&, Episode&);
private:
	int mViewers;
	double mSumScore;
	double mMaxScore;
	Description mDescription;
public:
	Episode(int, double, double, Description);
	std::string getName() const;
};

#endif