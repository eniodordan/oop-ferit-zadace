#ifndef SEASON_H
#define SEASON_H
#define _CRT_SECURE_NO_WARNINGS

class Season {
private:
	std::vector<Episode> mEpisodes;
public:
	Season(std::vector<Episode>&);
	~Season();
	Episode& operator[](int);
	const Episode& operator[](int) const;
	void add(Episode&);
	void remove(std::string);
	int getSize() const;
};

std::vector<Episode> loadEpisodesFromFile(std::string);

#endif