#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "description.h"
#include "episode.h"
#include "season.h"
#include "exception.h"
#include "printer.h"

// ---------------------------------------

Description::Description(int number, int lenght, std::string name) : mNumber(number), mLenght(lenght), mName(name) {}

std::ostream& operator<< (std::ostream& OutputStream, const Description& rhs) {
	OutputStream << rhs.mNumber << "," << rhs.mLenght << "," << rhs.mName;

	return OutputStream;
}

std::istream& operator>> (std::istream& InputStream, Description& rhs) {
	char notch;
	InputStream >> rhs.mNumber >> notch >> rhs.mLenght >> notch;
	getline(InputStream, rhs.mName);

	return InputStream;
}

std::string Description::getName() const { return this->mName; }

// ---------------------------------------

Episode::Episode(int viewers, double sumscore, double maxscore, Description description) : mViewers(viewers), mSumScore(sumscore), mMaxScore(maxscore), mDescription(description) {}

std::ostream& operator<< (std::ostream& OutputStream, const Episode& rhs) {
	OutputStream << rhs.mViewers << " ," << rhs.mSumScore << "," << rhs.mMaxScore << "," << rhs.mDescription;

	return OutputStream;
}

std::istream& operator>> (std::istream& InputStream, Episode& rhs) {
	char notch;
	InputStream >> rhs.mViewers >> notch >> rhs.mSumScore >> notch >> rhs.mMaxScore >> notch >> rhs.mDescription;

	return InputStream;
}

std::string Episode::getName() const { return this->mDescription.getName(); }

// ---------------------------------------

Episode& Season::operator[](int idx) {
	return this->mEpisodes[idx];
}

const Episode& Season::operator[](int idx) const {
	return this->mEpisodes[idx];
}

Season::Season(std::vector<Episode>& Episodes) {
	int size = Episodes.size();
	mEpisodes.reserve(size);
	for (int i = 0; i < size; i++) {
		mEpisodes.push_back(Episodes[i]);
	}
}

Season::~Season() { mEpisodes.clear(); }

void Season::add(Episode& Episode) {
	mEpisodes.push_back(Episode);
}

void Season::remove(std::string name) {
	int size = mEpisodes.size();
	for (int i = 0; i < size; i++) {
		if (mEpisodes[i].getName() == name) {
			mEpisodes.erase(mEpisodes.begin() + i);
		}
		else {
			throw EpisodeNotFoundException(name);
		}
	}
}

int Season::getSize() const {
	return mEpisodes.size();
}

std::vector<Episode> loadEpisodesFromFile(std::string fileName) {
	int i = 0;
	std::ifstream input(fileName);
	std::vector<Episode> Episodes;

	if (input.is_open() == false) {
		std::cout << "Error!" << std::endl;
	}

	while (!input.eof()) {
		input >> Episodes[i];
		i++;
	}
	input.close();

	return Episodes;
}

// ---------------------------------------

EpisodeNotFoundException::EpisodeNotFoundException(std::string name) : std::runtime_error("No such episode found.") {
	mName = name;
}

std::string EpisodeNotFoundException::getName() const { return this->mName; }

// ---------------------------------------

ConsolePrinter::ConsolePrinter() {}

void ConsolePrinter::print(Season& Episodes){
	int size = Episodes.getSize();

	std::cout << "******************************" << std::endl;
	for (int i = 0; i < size; i++) {
		std::cout << i << ". " << Episodes[i] << std::endl;;
	}
	std::cout << "******************************" << std::endl;
}

FilePrinter::FilePrinter(std::string filename) {
	fileObject = std::ofstream(filename, std::ios::app);
}

FilePrinter::~FilePrinter(){
	if (fileObject.is_open()){
		fileObject.close();
	}
}

void FilePrinter::print(Season& Episodes) {
	int size = Episodes.getSize();

	fileObject << "******************************\n";
	for (int i = 0; i < size; i++) {
		fileObject << i << ". " << Episodes[i] << "\n";
	}
	fileObject << "******************************\n";
}