#ifndef DESCRIPTION_H
#define DESCRIPTION_H
#define _CRT_SECURE_NO_WARNINGS

class Description {
	friend std::ostream& operator<< (std::ostream&, const Description&);
	friend std::istream& operator>> (std::istream&, Description&);
private:
	int mNumber;
	int mLenght;
	std::string mName;
public:
	Description(int, int, std::string);
	std::string getName() const;
};

#endif