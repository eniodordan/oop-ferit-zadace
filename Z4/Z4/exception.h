#ifndef EXCEPTION_H
#define EXCEPTION_H
#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
class EpisodeNotFoundException : public std::runtime_error {
private:
	std::string mName;
public:
	EpisodeNotFoundException(std::string);
	std::string getName() const;
};

#endif