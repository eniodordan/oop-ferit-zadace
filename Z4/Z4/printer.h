#ifndef PRINTER_H
#define PRINTER_H
#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include "season.h"

class IPrinter {
public:
	virtual void print(Season& Episodes) = 0;
	virtual ~IPrinter() {};
};

class ConsolePrinter : public IPrinter {
public:
	ConsolePrinter();
	virtual void print(Season&);
};class FilePrinter : public IPrinter {
private:
	std::ofstream fileObject;
public:
	FilePrinter(std::string);
	virtual ~FilePrinter();
	virtual void print(Season&);
};

#endif