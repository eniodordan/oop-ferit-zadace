#include <iostream>
#include <cstdlib>
#include <ctime>
#include "episode.h"

using namespace std;

Episode::Episode() : mViewers(0), mSumScore(0), mMaxScore(0)
{
}

Episode::Episode(int viewers, double sumscore, float maxscore) : mViewers(viewers), mSumScore(sumscore), mMaxScore(maxscore)
{
}

void Episode::addView(float score) {
	mViewers++;
	mSumScore += score;

	if (score > mMaxScore) {
		mMaxScore = score;
	}
}

float Episode::getMaxScore() const {
	return mMaxScore;
}

float Episode::getAverageScore() const {
	return (float)(mSumScore / mViewers);
}

int Episode::getViewerCount() const {
	return mViewers;
}

float generateRandomScore() {
	float random = (float)rand() * 10 / (float)RAND_MAX;
	return random;
}