#ifndef EPISODE_H
#define EPISODE_H
#define _CRT_SECURE_NO_WARNINGS

class Episode {
private:
	int mViewers;
	double mSumScore;
	float mMaxScore;
public:
	void addView(float);
	float getMaxScore() const;
	float getAverageScore() const;
	int getViewerCount() const;
	Episode();
	Episode(int, double, float);
};

float generateRandomScore();

#endif