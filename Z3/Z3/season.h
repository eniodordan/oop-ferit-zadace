#ifndef SEASON_H
#define SEASON_H
#define _CRT_SECURE_NO_WARNINGS

class Season {
private:
	Episode** mEpisodes;
	int mNumberOfEpisodes;
public:
	Season(Episode** ,const int);
	~Season();
	Season(const Season&);
	double getAverageRating() const;
	int getTotalViews() const;
	Episode* getBestEpisode() const;
	Episode& operator[](int);
	const Episode& operator[](int) const;
};

#endif