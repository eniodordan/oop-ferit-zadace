#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <fstream>
#include "description.h"
#include "episode.h"
#include "season.h"

using namespace std;

// ---------------------------------------

Season::Season(Episode** Episodes, const int NumberOfEpisodes) : mNumberOfEpisodes(NumberOfEpisodes)
{
	mEpisodes = new Episode*[mNumberOfEpisodes];
	for (int i = 0; i < mNumberOfEpisodes; i++) {
		mEpisodes[i] = Episodes[i];
	}
}

Season::~Season() { delete[] mEpisodes; }

Season::Season(const Season& ref) : mNumberOfEpisodes(ref.mNumberOfEpisodes) {
	mEpisodes = new Episode*[mNumberOfEpisodes];
	for (int i = 0; i < mNumberOfEpisodes; i++) {
		mEpisodes[i] = ref.mEpisodes[i];
	}
}

// ---------------------------------------

Episode& Season::operator[](int idx) {
	return *this->mEpisodes[idx];
}

const Episode& Season::operator[](int idx) const {
	return *this->mEpisodes[idx];
}
// ---------------------------------------

double Season::getAverageRating() const{
	int count = 0;
	double sum = 0;
	for (int i = 0; i < mNumberOfEpisodes; i++) {
		sum += mEpisodes[i]->getAverageScore();
		count++;
	}
	return sum / count;
}

int Season::getTotalViews() const {
	int total = 0;
	for (int i = 0; i < mNumberOfEpisodes; i++) {
		total += mEpisodes[i]->getViewerCount();
	}

	return total;
}

Episode* Season::getBestEpisode() const {
	Episode* BestEpisode;
	double maxscore;

	maxscore = mEpisodes[0]->getMaxScore();
	for (int i = 0; i < mNumberOfEpisodes; i++) {
		if (mEpisodes[i]->getMaxScore() > maxscore) {
			maxscore = mEpisodes[i]->getMaxScore();
			BestEpisode = mEpisodes[i];
		}
	}

	return BestEpisode;
}

// ---------------------------------------

Description::Description() : mNumber(0), mLenght(0), mName("")
{
}

Description::Description(int number, int lenght, string name) : mNumber(number), mLenght(lenght), mName(name)
{
}

// ---------------------------------------

std::ostream& operator<< (std::ostream& OutputStream, const Description& rhs) {
	OutputStream << rhs.mNumber << "," << rhs.mLenght << "," << rhs.mName;

	return OutputStream;
}

std::istream& operator>> (std::istream& InputStream, Description& rhs) {
	char notch;
	InputStream >> rhs.mNumber >> notch >> rhs.mLenght >> notch;
	getline(InputStream, rhs.mName);

	return InputStream;
}

bool operator> (const Description& lhs, const Description& rhs)
{
	return lhs.mName[0] > rhs.mName[0];
}

bool operator== (const Description& lhs, const Description& rhs)
{
	return lhs.mNumber == rhs.mNumber && lhs.mLenght == rhs.mLenght && lhs.mName == rhs.mName;
}

// ---------------------------------------

Episode::Episode() : mViewers(0), mSumScore(0), mMaxScore(0)
{
}

Episode::Episode(int viewers, double sumscore, double maxscore, Description description) : mViewers(viewers), mSumScore(sumscore), mMaxScore(maxscore), mDescription(description)
{
}

// ---------------------------------------

std::ostream& operator<< (std::ostream& OutputStream, const Episode& rhs) {
	OutputStream << rhs.mViewers << " ," << rhs.mSumScore << "," << rhs.mMaxScore << "," << rhs.mDescription;

	return OutputStream;
}

std::istream& operator>> (std::istream& InputStream, Episode& rhs) {
	char notch;
	InputStream >> rhs.mViewers >> notch >> rhs.mSumScore >> notch >> rhs.mMaxScore >> notch >> rhs.mDescription;

	return InputStream;
}

bool operator> (const Episode& lhs, const Episode& rhs)
{
	return lhs.mDescription > rhs.mDescription;
}

bool operator== (const Episode& lhs, const Episode& rhs)
{
	return lhs.mViewers == rhs.mViewers && lhs.mSumScore == rhs.mSumScore && lhs.mMaxScore == rhs.mMaxScore && lhs.mDescription == rhs.mDescription;
}

/* Episode& Episode::operator=(const Episode& Ref)
{
	if (&Ref == this) return *this;
	this->mViewers = Ref.mViewers;
	this->mSumScore = Ref.mSumScore;
	this->mMaxScore = Ref.mMaxScore;
	this->mDescription = Ref.mDescription;
	return *this;
}*/

// ---------------------------------------

void Episode::addView(float score) {
	mViewers++;
	mSumScore += score;

	if (score > mMaxScore) {
		mMaxScore = score;
	}
}

double Episode::getMaxScore() const {
	return mMaxScore;
}

double Episode::getAverageScore() const {
	return (float)(mSumScore / mViewers);
}

int Episode::getViewerCount() const {
	return mViewers;
}

// ---------------------------------------

float generateRandomScore() {
	float random = (float)rand() * 10 / (float)RAND_MAX;
	return random;
}

void print(Episode *episode[], const int count) {
	for (int i = 0; i < count; i++) {
		cout << i << ". " << *episode[i] << endl;
	}
}

void swap(Episode *xp, Episode *yp)
{
	Episode temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void sort(Episode *episode[], const int count) {
	int i, j;
	for (i = 0; i < count - 1; i++) {
		for (j = 0; j < count - i - 1; j++) {
			if (*episode[j] > *episode[j + 1]) {
				swap(episode[j], episode[j + 1]);
			}
		}
	}
}

void persistToFile(string fileName, Episode *episode[], const int count) {
	ofstream output(fileName);
	if (output.is_open() == false) {
		cout << "Error!" << endl;
	}
	else {
		for (int i = 0; i < count; i++) {
			output << i << ". " << *episode[i] << "\n";
		}
	}
	output.close();
}

Episode** loadEpisodesFromFile(string fileName, const int count) {
	std::ifstream input(fileName);
	Episode* episodes[10]; // Array size should be known at compile time, so variable count it's not going to work.

	if (input.is_open() == false) {
		cout << "Error!" << endl;
	}

	for (int i = 0; i < count; i++) {
		episodes[i] = new Episode();
		input >> *episodes[i];
	}
	input.close();

	return episodes;
}